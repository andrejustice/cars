package cars.com.example.cars.model;

public enum RoleEnum {
    ROLE_CUSTOMER,
    ROLE_ADMIN
}
