package cars.com.example.cars.model;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "car")
public class Car {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long car_id;

    @NotBlank
	@Size(max = 30)
	private String car_name;

    @NotBlank
	@Size(max = 30)
	private String car_price;

    private String car_image;

    @ManyToOne
    @JoinColumn(name = "brand_id")
	private Brand brand_id;
}
