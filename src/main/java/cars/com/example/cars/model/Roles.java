package cars.com.example.cars.model;

import javax.persistence.*;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "roles")
public class Roles {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long roles_id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private RoleEnum name;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	private Set<Users> users;
}
