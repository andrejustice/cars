package cars.com.example.cars.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "brand")
public class Brand {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long brand_id;

    @Size(max = 20)
	private String brandName;
}
